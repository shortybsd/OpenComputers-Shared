-----------------------------------------
-- Simple AR Glasses Demo by shortybsd --
-----------------------------------------
--
-- This will get the info from an Ender IO capacitor bank.
-- Setup an OpenComputers Computer Case next to a Glasses Terminal.
-- Place an Adapter next to an Ender IO capacitor bank then run OpenComputers
-- cable back to Computer Case.

local component = require("component")
local g = component.glasses
local cb = component.capacitor_bank

-- Clear / Reset all objects
g.removeAll()

-- Initialize objects
box1 = g.addRect()
lbl1 = g.addTextLabel()
lbl2 = g.addTextLabel()

-- Set the properties of your objects
box1.setPosition(1,100)
box1.setSize(18,80)
-- Alpha: 0 = fully transparent, 1 = fully opaque
box1.setAlpha(0.6)
lbl1.setPosition(4,210)
lbl1.setScale(0.5)
lbl1.setColor(0,1,0)
lbl2.setPosition(4,220)
lbl2.setScale(0.5)
lbl2.setColor(1,1,0)

while true do
  local energyStored = cb.getEnergyStored()
  local maxEnergy = cb.getMaxEnergyStored()
  local per = math.floor(energyStored/maxEnergy*100,0)
  lbl1.setText("Battery Bank: "..energyStored)
  lbl2.setText("Percentage: "..per.."%")
  os.sleep(1)
end